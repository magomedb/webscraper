const puppeteer = require('puppeteer');
const fs = require('fs');
const URL = 'http://books.toscrape.com';

async function scrape(url) {
    const browser = await puppeteer.launch();

    try {
        const page = await browser.newPage();
        await page.goto(url, {waitUntil: 'networkidle0'});

        let results = [];
        let elements = await page.$$('ol[class="row"] > li');

        for(let element of elements) {
            let title = await element.$eval(('article[class="product_pod"] > h3 > a'), node => node.getAttribute("title"));
            let price = await element.$eval(('div[class="product_price"] > p'), node => node.innerText.trim());
            results.push({title, price});
        }
        return JSON.stringify(results);
    } catch (error) {
        console.log(error);
    } finally {
        browser.close();
    }
}

// Wait for scrape to finish, then write to file
(async function writeScraped() {
    fs.writeFile('scraped.json', await scrape(URL), err => {
        if(err) console.log(err);
    });
})();