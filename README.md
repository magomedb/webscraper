# Autumn 2020 webscraper
A simple webscraper made with Node and Puppeteer.
AUTHOR: Magomed Bakhmadov
INSTALL:
1. Run 'npm install' to install required packages.
2. Run 'npm start' to scrape page and save values to file.